package com.example.tarea1nivel2.adaptador

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.tarea1nivel2.R
import com.example.tarea1nivel2.model.ContactEntity

class ContactoAdapter(private val context: Context, private val contacts:List<ContactEntity>): BaseAdapter()  {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.row_contacto, null)
        val imgContact = container.findViewById<ImageView>(R.id.iviContact)
        val tviName = container.findViewById<TextView>(R.id.tvnombrepersona)

        //Extraer la entidad
        val contactEntity = contacts[position]

        //Asociar la entidad con el XML
        tviName.text=contactEntity.name
        imgContact.setImageResource(contactEntity.foto)

        return container
    }

    override fun getItem(position: Int)= contacts[position]

    override fun getItemId(position: Int):Long=0

    override fun getCount():Int = contacts.size
}