package com.example.tarea1nivel2.fragmentos

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tarea1nivel2.R
import com.example.tarea1nivel2.adaptador.ContactoAdapter
import com.example.tarea1nivel2.contactos.OnContactListener
import com.example.tarea1nivel2.model.ContactEntity
import kotlinx.android.synthetic.main.fragment_main_content_contacs.*

class MainContentContacsFragment : Fragment() {
    private var listener :OnContactListener?=null
    private  var contacts = mutableListOf<ContactEntity>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_content_contacs, container, false)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnContactListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()
        context?.let {
            lstContacts.adapter=ContactoAdapter(it,contacts)
        }
        lstContacts.setOnItemClickListener{ _,_,i,_ ->
            listener?.let {
                it.onSelectedItemContact(contacts[i])
            }
        }
    }

    private fun setData() {
        val contact1 = ContactEntity(1,"Akira Nizama Durand","(01)-2267730",R.drawable.profilefb,"akiranizama@gmail.com","www.google.com" )
        val contact2 = ContactEntity(2,"Akemi Nizama Durand","(01)-2267730",R.drawable.profilefb,"akeminizama@gmail.com","www.google.com")
        val contact3 = ContactEntity(3,"Raul Nizama Durand","(01)-2267730",R.drawable.profilefb,"raulnizama@gmail.com","www.google.com")
        val contact4 = ContactEntity(4,"Norma Nizama Durand","(01)-2267730",R.drawable.profilefb,"normanizama@gmail.com","www.google.com")

        contacts.add(contact1)
        contacts.add(contact2)
        contacts.add(contact3)
        contacts.add(contact4)


    }

    private fun first(): ContactEntity? {
        contacts?.let {
            return it[0]
        }
        return null
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

        fun getUrlFromIntent() {
        val url = "www.google.com"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

}