package com.example.tarea1nivel2.fragmentos

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tarea1nivel2.R
import com.example.tarea1nivel2.contactos.OnContactListener
import com.example.tarea1nivel2.model.ContactEntity
import kotlinx.android.synthetic.main.fragment_detail_contact.*


class DetailContactFragment : Fragment() {
    private var listener :OnContactListener?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_contact, container, false)
    }
    fun renderContact(contactEntity: ContactEntity) {
        val name = contactEntity.name
        val numero = contactEntity.telf
        val email = contactEntity.correo
        val refweb = contactEntity.refprofile

        tviName.text = name
        tvnumero.text = numero
        rptacorreo.text = email
        idwebsite.text=refweb
        iviProduct.setImageResource(contactEntity.foto)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnContactListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnContactListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    }
