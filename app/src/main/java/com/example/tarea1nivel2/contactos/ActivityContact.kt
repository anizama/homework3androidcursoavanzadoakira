package com.example.tarea1nivel2.contactos

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.example.tarea1nivel2.R
import com.example.tarea1nivel2.fragmentos.DetailContactFragment
import com.example.tarea1nivel2.fragmentos.MainContentContacsFragment
import com.example.tarea1nivel2.model.ContactEntity
import kotlinx.android.synthetic.main.activity_contact.*

class ActivityContact:AppCompatActivity(),OnContactListener {
    private lateinit var frameNameContacsFragment: MainContentContacsFragment
    private lateinit var frameDetailContactFragment: DetailContactFragment
    private lateinit var fragmentManager: FragmentManager
    override fun onSendMessage(msg: String) {

    }

    override fun onSelectedItemContact(contactEntity: ContactEntity) {
       frameDetailContactFragment.renderContact(contactEntity)
    }

    override fun renderfirst(contactEntity: ContactEntity?) {
        contactEntity?.let {
            onSelectedItemContact(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        fragmentManager= supportFragmentManager
        frameNameContacsFragment= fragmentManager.findFragmentById(R.id.fragmentNameContacts) as MainContentContacsFragment
        frameDetailContactFragment= fragmentManager.findFragmentById(R.id.fragContactDetail) as DetailContactFragment
   //     setupHyperlink()


    }

    //private fun setupHyperlink() {
      //  val linkTextView = findViewById<TextView>(R.id.fragContactDetail)
       // linkTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }
