package com.example.tarea1nivel2.contactos
import com.example.tarea1nivel2.model.ContactEntity

interface OnContactListener {
    fun onSendMessage(msg:String)
    fun onSelectedItemContact(contactEntity: ContactEntity)
    fun renderfirst(contactEntity: ContactEntity?)
}