package com.example.tarea1nivel2.model

import java.io.Serializable

data class ContactEntity(val id:Int, val name:String, val telf:String,
                         val foto:Int, val correo:String,val refprofile:String):Serializable {
}